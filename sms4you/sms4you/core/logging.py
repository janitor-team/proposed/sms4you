#!/usr/bin/python3

# Copyright (C) 2019-2020
#
# * Felix Delattre <felix@delattre.de>
# * W. Martin Borgert <debacle@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import logging
import systemd.journal


class Logging(object):
    def __init__(self, debug):
        self.log = logging.getLogger("sms4you")
        self.log.addHandler(systemd.journal.JournalHandler())
        self.log.setLevel(logging.DEBUG if debug else logging.INFO)

        if debug:
            log_stdout_handler = logging.StreamHandler(sys.stdout)
            self.log.addHandler(log_stdout_handler)
