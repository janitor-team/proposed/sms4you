=======
sms4you
=======
-----------------------------------------------------------
Personal gateway connecting SMS with **XMPP** or **email**.
-----------------------------------------------------------

``sms4you`` forwards messages from and to SMS and connects either with
``sms4you-xmpp`` or ``sms4you-email`` to choose the other mean of communication.
Nice for receiving or sending SMS, independently from carrying a SIM card.

.. figure:: ./sms4you-raspberry-pi.jpg
   :alt: sms4you on a Raspberry Pi

Idea
----

Connect a `suitable GSM modem or phone <https://www.freedesktop.org/wiki/Software/ModemManager/SupportedDevices/>`__
containing a SIM card into a computer (e.g. a Raspberry Pi) at a fixed 
place. Give ``sms4you`` access to either a simple email address or a 
small xmpp server. It will use this connection to receive emails or 
xmpp messages to be sent out as SMS and to send out emails or xmpp 
messages with received SMS messages.

Concretely - why?
-----------------

There can be many reasons, why you want to use ``sms4you``. Here are
some examples:

-  You are living in the internetz, and you just need a gateway to this
   old thing "SMS", which some people really still seem to use.
-  You travel and use local SIM cards, but you still want to be able to
   receive SMS confirmation codes from banks and services.
-  You don't want to carry a (registered) SIM card for `good
   reasons <https://www.theguardian.com/technology/2016/apr/19/ss7-hack-us-congressman-calls-texts-location-snooping>`__,
   but still you want to be able to send and receive SMS.

Requirements
------------

``sms4you`` is implemented as a `Python <https://www.python.org/>`__
daemon, using `Asynchronous I/O <https://docs.python.org/3/library/asyncio.html>`__. 
For the connection with the modem it uses 
`ModemManager <https://www.freedesktop.org/wiki/Software/ModemManager/>`__.
The different gateways for email and xmpp have different depenedencies and
are wrapped therefore into different sub-projects:

Installation
------------

-  As an `email gateway <./docs/README.email.rst>`__
-  As a `xmpp gateway <./docs/README.xmpp.rst>`__

Run sms4you as unprivileged user
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

   $ sudo addgroup --force-badname --system _sms4you
   $ sudo adduser --force-badname --home /nonexistent \
         --ingroup _sms4you --no-create-home --system _sms4you
   $ sudo cp sms4you.conf /etc/dbus-1/system.d/
   $ sudo cp sms4you.pkla /var/lib/polkit-1/localauthority/30-site.d/
   $ sudo chown root:_sms4you /etc/sms4you/{sim_pin,xmpp_component_password}
   $ sudo chmod 640 /etc/sms4you/{sim_pin,xmpp_component_password}

Copyright and copyleft
----------------------

Copyright (C) 2019-2020

* Felix Delattre felix@delattre.de
* Martin debacle@debian.org

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see http://www.gnu.org/licenses/.

Also add information on how to contact you by electronic and paper mail.

If your software can interact with users remotely through a computer
network, you should also make sure that it provides a way for users to
get its source. For example, if your program is a web application, its
interface could display a "Source" link that leads users to an archive
of the code. There are many ways you could offer source, and different
solutions will be better for different programs; see section 13 for the
specific requirements.

See the `LICENSE <./LICENSE>`__ for the full license text.
